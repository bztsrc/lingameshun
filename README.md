Linuxos játékok MAGYARUL
========================

Ebbe a repoba gyújtöm az általam lefordított Linuxos játékok szótárait.
A főkönyvtár mindig teljes elérési utat tartalmaz, azaz mindig a gyökérkönyvtárba
kell másolni a tartalmát. Az etr (Extreme Tux Racer) nem az igazi, mivel
nem találtam megfelelő fontot, így az eredményhirdetésnél hiányoznak az ékezetek.
A többi teljesen jó.

Üdv,

bzt
